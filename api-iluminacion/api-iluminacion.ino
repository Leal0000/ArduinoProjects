#include <ArduinoJson.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SoftwareSerial.h>
#define registro 5
SoftwareSerial s(12,14);
WiFiClient wificlient;
const char* ssid = "IZZI-JUAN PABLO";
const char* password =  "24442654";
/*
int pin_Red = 16;
int pin_Green = 05;
int pin_Blue = 04;
*/
int pin_Red = 4;
int pin_Green = 0;
int pin_Blue = 2;
void setup() {
  delay(10);
  s.begin(9600);
  Serial.begin(9600);
  pinMode(pin_Red, OUTPUT);
  pinMode(pin_Green, OUTPUT);
  pinMode(pin_Blue, OUTPUT);
  WiFi.begin(ssid, password);
  Serial.print("Conectando...");
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(500);
    Serial.print(".");
  }
  Serial.print("Conectado con éxito, mi IP es: ");
  Serial.println(WiFi.localIP());

}

void loop() {

  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    HTTPClient http;
    http.begin(wificlient,"http://heroku-flas.herokuapp.com/api/device_colors/44");        //Indicamos el destino
    http.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Preparamos el header text/plain si solo vamos a enviar texto plano sin un paradigma llave:valor.
    int codigo_respuesta = http.GET();   //Enviamos el post pasándole, los datos que queremos enviar. (esta función nos devuelve un código que guardamos en un int)
    if(codigo_respuesta>0){
      Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code
      if(codigo_respuesta == 200){
        String cuerpo_respuesta = http.getString();
        Serial.println("El servidor respondió ▼ ");
        Serial.println(cuerpo_respuesta);
        StaticJsonBuffer<1000> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(cuerpo_respuesta);
        Serial.println("JSON received and parsed");
        root.prettyPrintTo(Serial);
        Serial.println("");
        Serial.print("ID: ");
        int data = root["id"];
        Serial.println(data);
        int red = root["red"];
        int green = root["green"];
        int blue = root["blue"];
        Serial.print("red: ");
        Serial.println(red);
        Serial.print("green: ");
        Serial.println(green);
        Serial.print("blue: ");
        Serial.println(blue);
        Serial.println("");
        Serial.println("---------------------xxxxx--------------------");
        Serial.println("");
        analogWrite(pin_Red, red);
        analogWrite(pin_Green, green);
        analogWrite(pin_Blue, blue);
        /*
        JsonObject& root1 = jsonBuffer.createObject();
        root1["id"] = data;
        root1["red"] = red;
        root1["green"] = green;
        root1["blue"] = blue;
        root1.printTo(s);*/
      }
    }else{
     Serial.print("Error enviando POST, código: ");
     Serial.println(codigo_respuesta);
    }
    http.end();  //libero recursos
  }else{
     Serial.println("Error en la conexión WIFI");

  }

   delay(2000);
}
