#include <ArduinoJson.h>
#include <SoftwareSerial.h>
#define relepin 8
SoftwareSerial s(5,6);
void setup() {
  s.begin(9600);
  Serial.begin(9600);
  pinMode(relepin, OUTPUT);
}
  

void loop(){
  if(s.available() > 0){
    StaticJsonBuffer<1000> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(s);
    if(root != JsonObject::invalid()){
      Serial.println("JSON received and parsed");
      root.prettyPrintTo(Serial);
      Serial.println("");
      Serial.print("state");
      boolean data1=root["state"];
      Serial.println(data1);
      Serial.print("Heat-index  ");
      Serial.println("");
      Serial.println("---------------------xxxxx--------------------");
      Serial.println("");
      if(data1 != true){
        digitalWrite(relepin, HIGH);
      }
      else{
        digitalWrite(relepin, LOW);
      }
    }
  }
}
