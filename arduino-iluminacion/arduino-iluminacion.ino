#include <ArduinoJson.h>
#include <SoftwareSerial.h>
#define relepin 8
SoftwareSerial s(5,6);
void setup() {
  s.begin(9600);
  Serial.begin(9600);
  pinMode(relepin, OUTPUT);
}
  

void loop(){
  if(s.available() > 0){
    StaticJsonBuffer<1000> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(s);
    if(root != JsonObject::invalid()){
      Serial.println("JSON received and parsed");
      root.prettyPrintTo(Serial);
      Serial.println("");
      Serial.print("ID: ");
      int data1=root["id"];
      Serial.println(data1);
      int red = root["red"];
      int green = root["green"];
      int blue = root["blue"];
      Serial.print("red: ");
      Serial.println(red);
      Serial.print("green: ");
      Serial.println(green);
      Serial.print("blue: ");
      Serial.println(blue);
      Serial.print("Heat-index  ");
      Serial.println("");
      Serial.println("---------------------xxxxx--------------------");
      Serial.println("");
    }
  }
}
