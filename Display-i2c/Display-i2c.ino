#include <LiquidCrystal_I2C.h>// libreria para el i2c
LiquidCrystal_I2C lcd(0x27,16,4);// configuramos el I2C para un modulo de 16x2 que trabaja con el protocolo 0x27 o 0x20

void setup() {
  lcd.init();//la inciamos 
  lcd.backlight();//activamos el lcd de la pantalla
  lcd.clear();//la limpiamos 
  lcd.setCursor(0,0);//le decimos en que punto empezara 
  lcd.print("HOLA MUNDO :)");//imprimimos el valor entre comillas
  delay(3000);//esperamos 3 segundos
}

void loop() {
  for(int c=0; c<12;c++){
    lcd.scrollDisplayLeft();
    delay(400);
  }
  for(int c=0;c<12; c++){
    lcd.scrollDisplayRight();
    delay(400);
  }
  
}
