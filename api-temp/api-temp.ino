#include <ArduinoJson.h>
#include <DHT_U.h>
#include <DHT.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#define registro 5
DHT dht(registro,DHT11);
WiFiClient wificlient;
const char* ssid = "IZZI-JUAN PABLO";
const char* password =  "24442654";
void setup() {
  delay(10);
  Serial.begin(9600);
  dht.begin();
  WiFi.begin(ssid, password);
  Serial.print("Conectando...");
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(500);
    Serial.print(".");
  }
  Serial.print("Conectado con éxito, mi IP es: ");
  Serial.println(WiFi.localIP());

}

void loop() {

  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    HTTPClient http;
    float temp = dht.readTemperature();
    float hum = dht.readHumidity();
    //String datos_a_enviar = "user=" + user + "&password=" + pass;
    String datos_a_enviar = "temp=" + String(temp) + "&humd=" + String(hum);    \
    http.begin(wificlient,"http://heroku-flas.herokuapp.com/temperatura");        //Indicamos el destino
    http.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Preparamos el header text/plain si solo vamos a enviar texto plano sin un paradigma llave:valor.
    int codigo_respuesta = http.POST(datos_a_enviar);   //Enviamos el post pasándole, los datos que queremos enviar. (esta función nos devuelve un código que guardamos en un int)
    if(codigo_respuesta>0){
      Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code
      if(codigo_respuesta == 200){
        String cuerpo_respuesta = http.getString();
        Serial.println("El servidor respondió ▼ ");
        Serial.println(cuerpo_respuesta);
        StaticJsonBuffer<1000> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(cuerpo_respuesta);
        Serial.println("JSON received and parsed");
        root.prettyPrintTo(Serial);
        Serial.println("");
        Serial.print("Temperature ");
        float data1=root["temp"];
        Serial.println(data1);
        Serial.print("Humidity    ");
        float data2=root["humd"];
        Serial.println(data2);
        Serial.print("Heat-index  ");
        Serial.println("");
        Serial.println("---------------------xxxxx--------------------");
        Serial.println("");
      }
    }else{
     Serial.print("Error enviando POST, código: ");
     Serial.println(codigo_respuesta);
    }
    http.end();  //libero recursos
  }else{
     Serial.println("Error en la conexión WIFI");

  }

   delay(2000);
}
