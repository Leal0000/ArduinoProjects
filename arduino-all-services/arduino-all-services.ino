#include <ArduinoJson.h>
#include <SoftwareSerial.h>
#define relepin 8
SoftwareSerial s(5,6);
int pin_Red = 11;
int pin_Green = 10;
int pin_Blue = 9;
void setup() {
  s.begin(9600);
  Serial.begin(9600);
  pinMode(relepin, OUTPUT);
  pinMode(pin_Red, OUTPUT);
  pinMode(pin_Green, OUTPUT);
  pinMode(pin_Blue, OUTPUT);
}
  

void loop(){
  if(s.available() > 0){
    StaticJsonBuffer<1000> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(s);
    if(root != JsonObject::invalid()){
      Serial.println("JSON received and parsed");
      root.prettyPrintTo(Serial);
      Serial.println("");
      Serial.print("state:");
      boolean data1=root["state"];
      Serial.println(data1);
      int red = root["red"];
      int green = root["green"];
      int blue = root["blue"];
      Serial.print("red: ");
      Serial.println(red);
      Serial.print("green: ");
      Serial.println(green);
      Serial.print("blue: ");
      Serial.println(blue);
      Serial.print("Temperature: ");
      float data2=root["temp"];
      Serial.println(data2);
      Serial.print("Humidity: ");
      float data3=root["humd"];
      Serial.println(data3);
      Serial.print("Heat-index  ");
      Serial.println("");
      Serial.println("---------------------xxxxx--------------------");
      Serial.println("");
      if(data1 == true){
        digitalWrite(relepin, LOW);
      }
      else{
        digitalWrite(relepin, HIGH);
      }
      analogWrite(pin_Red, red);
      analogWrite(pin_Green, green);
      analogWrite(pin_Blue, blue);
    }
  }
}
