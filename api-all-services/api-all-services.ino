#include <ArduinoJson.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SoftwareSerial.h>
#define registro 5
SoftwareSerial s(12,14);
WiFiClient wificlient;
const char* ssid = "IZZI-JUAN PABLO";
const char* password =  "24442654";
int pin_Red = 16;
int pin_Green = 05;
int pin_Blue = 04;
void setup() {
  delay(10);
  s.begin(9600);
  Serial.begin(9600);
  pinMode(pin_Red, OUTPUT);
  pinMode(pin_Green, OUTPUT);
  pinMode(pin_Blue, OUTPUT);
  WiFi.begin(ssid, password);
  Serial.print("Conectando...");
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(500);
    Serial.print(".");
  }
  Serial.print("Conectado con éxito, mi IP es: ");
  Serial.println(WiFi.localIP());

}

void loop() {

  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    HTTPClient http;
    http.begin(wificlient,"http://heroku-flas.herokuapp.com/api/all_services/1");        //Indicamos el destino
    http.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Preparamos el header text/plain si solo vamos a enviar texto plano sin un paradigma llave:valor.
    int codigo_respuesta = http.GET();   //Enviamos el post pasándole, los datos que queremos enviar. (esta función nos devuelve un código que guardamos en un int)
    if(codigo_respuesta>0){
      Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code
      if(codigo_respuesta == 200){
        String cuerpo_respuesta = http.getString();
        Serial.println("El servidor respondió ▼ ");
        Serial.println(cuerpo_respuesta);
        StaticJsonBuffer<1000> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(cuerpo_respuesta);
        Serial.println("JSON received and parsed");
        root.prettyPrintTo(Serial);
        Serial.println("");
        Serial.print("Color: ");  
        String  color = root["color"];
        Serial.println(color);
        int red = root["color"]["red"];
        int green = root["color"]["green"];
        int blue = root["color"]["blue"];
        Serial.print("red: ");
        Serial.println(red);
        Serial.print("green: ");
        Serial.println(green);
        Serial.print("blue: ");
        Serial.println(blue);
        Serial.print("State: ");
        String  State = root["state"];
        Serial.println(State);
        Serial.print("Status: ");
        boolean data = root["state"]["state"];
        Serial.println(data);
        Serial.print("temps: ");
        String  temp = root["temp"];
        Serial.println(temp);
        Serial.print("Temperature ");
        float data1=root["temp"]["temp"];
        Serial.println(data1);
        Serial.print("Humidity    ");
        float data2=root["temp"]["humd"];
        Serial.println(data2);
        Serial.print("Heat-index  ");
        Serial.println("");
        Serial.println("---------------------xxxxx--------------------");
        Serial.println("");
        JsonObject& root1 = jsonBuffer.createObject();
        root1["state"] = data;
        root1["red"] = red;
        root1["green"] = green;
        root1["blue"] = blue;
        root1["temp"] = data1;
        root1["humd"] = data2;
        root1.printTo(s);
      
      }
    }else{
     Serial.print("Error enviando POST, código: ");
     Serial.println(codigo_respuesta);
    }
    http.end();  //libero recursos
  }else{
     Serial.println("Error en la conexión WIFI");

  }

   delay(2000);
}
