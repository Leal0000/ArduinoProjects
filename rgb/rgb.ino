int pin_Red = 16;
int pin_Green = 05;
int pin_Blue = 04;
int i;

void setup() {
  Serial.begin(9600);
  pinMode(pin_Red, OUTPUT);
  pinMode(pin_Green, OUTPUT);
  pinMode(pin_Blue, OUTPUT);
}

void loop() {
  /*
  for(i = 0; i<256; i++){
    analogWrite(pin_Blue, i);
    Serial.println(i);
  }
  for(i=255; i>0; i--){
    analogWrite(pin_Blue, i);
    Serial.println(i);
  }*/
  analogWrite(pin_Red, 255);
  analogWrite(pin_Green, 255);
  analogWrite(pin_Blue, 0);
}
