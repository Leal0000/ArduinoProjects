#include <ArduinoJson.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SoftwareSerial.h>
#define registro 5
SoftwareSerial s(12,14);
WiFiClient wificlient;
const char* ssid = "IZZI-JUAN PABLO";
const char* password =  "24442654";
const String device_id = "1";
int pinled = 5;
void setup() {
  delay(10);
  s.begin(9600);
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  Serial.print("Conectando...");
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(500);
    Serial.print(".");
  }
  Serial.print("Conectado con éxito, mi IP es: ");
  Serial.println(WiFi.localIP());
  pinMode(pinled, OUTPUT);

}

void loop() {

  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    HTTPClient http;
    //http.begin(wificlient,"http://heroku-flas.herokuapp.com/api/device_status/2");        //Indicamos el destino
    http.begin(wificlient,"http://app-flask-graphql.herokuapp.com/api/switch/" + device_id);        //Indicamos el destino
    http.addHeader("Content-Type","application/json"); //Preparamos el header text/plain si solo vamos a enviar texto plano sin un paradigma llave:valor.
    http.addHeader("Accept","application/json");
    http.addHeader("Access-Control-Allow-Origin","*");
    http.addHeader("body","{query : query{ switch(id:1){status state message}}}");
    int codigo_respuesta = http.GET();   //Enviamos el post pasándole, los datos que queremos enviar. (esta función nos devuelve un código que guardamos en un int)
    if(codigo_respuesta>0){
      Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code
      if(codigo_respuesta == 200){
        String cuerpo_respuesta = http.getString();
        Serial.println("El servidor respondió ▼ ");
        Serial.println(cuerpo_respuesta);
        StaticJsonBuffer<1000> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(cuerpo_respuesta);
        Serial.println("JSON received and parsed");
        root.prettyPrintTo(Serial);
        Serial.println("");
        Serial.print("Status");
        boolean data = root["state"];
        Serial.println(data);
        Serial.println("");
        Serial.println("---------------------xxxxx--------------------");
        Serial.println("");
        JsonObject& root1 = jsonBuffer.createObject();
        root1["state"] = data;
        root1.printTo(s);
        if(data == true){
          digitalWrite(pinled, HIGH);
        }
        else{
          digitalWrite(pinled, LOW);
        }
      }
    }else{
     Serial.print("Error enviando POST, código: ");
     Serial.println(codigo_respuesta);
    }
    http.end();  //libero recursos
  }else{
     Serial.println("Error en la conexión WIFI");

  }

   delay(2000);
}
